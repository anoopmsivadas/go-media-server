package hls

import (
	"go-media-server/pkg/models"
	"go-media-server/pkg/web/config"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const ExtractionDirName = "hls_cache"

type TranscodingError struct {
	Resolution string
	Error      error
}

func cmdHLS360p(videoFilePath, destDir string) []string {
	// see https://superuser.com/questions/624563/how-to-resize-a-video-to-make-it-smaller-with-ffmpeg
	return []string{
		"-hide_banner",
		"-y",
		"-i", videoFilePath,
		"-vf", "scale=trunc(oh*a/2)*2:360",
		"-c:a", "aac",
		"-ar", "48000",
		"-c:v", "h264", // codec:video output format
		"-profile:v", "main",
		"-crf", "20", // video quality 1 the best, 51 worst
		"-sc_threshold", "0",
		"-g", "48",
		"-keyint_min", "48",
		"-hls_time", "10",
		"-hls_playlist_type", "vod",
		"-b:v", "800k",
		"-maxrate", "856k",
		"-bufsize", "1200k",
		"-b:a", "96k",
		"-hls_segment_filename", filepath.Join(destDir, "360p_%03d.ts"),
		filepath.Join(destDir, "360p.m3u8"),
	}
}

func createm3u8Playlist(path string, res []string) {
	f, _ := os.Create(filepath.Join(path, "playlist.m3u8"))
	defer f.Close()

	c := `#EXTM3U
	#EXT-X-VERSION:3
	`

	for _, r := range res {
		if r == "360p" {
			c = c + `#EXT-X-STREAM-INF:BANDWIDTH=800000,RESOLUTION=640x360
	360p.m3u8
	`
		}

		if r == "480p" {
			c = c + `#EXT-X-STREAM-INF:BANDWIDTH=1400000,RESOLUTION=842x480
	480p.m3u8
	`
		}

		if r == "720p" {
			c = c + `#EXT-X-STREAM-INF:BANDWIDTH=2800000,RESOLUTION=1280x720
	720p.m3u8
	`
		}

		if r == "1080p" {
			c = c + `#EXT-X-STREAM-INF:BANDWIDTH=5000000,RESOLUTION=1920x1080
	1080p.m3u8
	`
		}
	}

	f.Write([]byte(c))
}

func GenerateHLSFiles(mediaPath string, destDir string, ffmpegHome string) (bool, []TranscodingError) {

	availableResolutions := map[string]func(string, string) []string{
		"360p": cmdHLS360p,
	}
	//TEMP
	reso := []string{"360p"}

	resolutions := make(map[string]func(string, string) []string)
	for _, r := range reso {
		if availableResolutions[r] != nil {
			resolutions[r] = availableResolutions[r]
		}
	}

	createm3u8Playlist(destDir, reso)

	output := make(chan TranscodingError, len(resolutions))
	for reso, cmdStrings := range resolutions {
		go func(out chan<- TranscodingError, commandProducer func(string, string) []string, resolution string) {
			cmd := exec.Command(ffmpegHome, commandProducer(mediaPath, destDir)...)
			cmd.Stdout = log.Writer()
			cmd.Stderr = log.Writer()

			log.Println(strings.Join(cmd.Args, " "))

			err := cmd.Start()

			if err != nil {
				log.Fatalln("Exec error:", err)
				out <- TranscodingError{
					Resolution: resolution,
					Error:      err,
				}
				return
			}

			out <- TranscodingError{
				Resolution: resolution,
				Error:      nil,
			}
		}(output, cmdStrings, reso)

	}
	var hasError bool
	errors := make([]TranscodingError, len(resolutions))

	iter := 0
	for out := range output {
		if out.Error != nil && !hasError {
			hasError = true
		}
		errors = append(errors, out)
		iter++

		if iter == len(resolutions) {
			close(output)
		}
	}

	return hasError, errors
}

func getExtractionMovieDir(appDir string, mediaName string) string {
	cacheDir, _ := os.UserCacheDir()
	return filepath.Join(cacheDir, appDir, ExtractionDirName, mediaName)
}

func createWriteableDir(path string) error {
	return os.MkdirAll(path, 0777)
}

func DoExtraction(video *models.Video, config *config.APIConfig) (bool, []TranscodingError) {
	extractionDirName := getExtractionMovieDir(config.AppHome, video.FileName)

	if err := createWriteableDir(extractionDirName); err != nil {
		return true, nil
	}

	return GenerateHLSFiles(
		filepath.Join(video.FilePath, video.FileName),
		extractionDirName,
		config.FFMPEGBin,
	)
}
