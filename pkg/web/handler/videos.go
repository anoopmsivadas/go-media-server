package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"go-media-server/pkg/web/config"
	"log"
	"net/http"
	"os/exec"
)

func VideoHandler(config config.APIConfig) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		cmd := exec.Command("pwd")
		var outb, errb bytes.Buffer
		cmd.Stdout = &outb
		cmd.Stderr = &errb
		err := cmd.Run()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("out:", outb.String(), "err:", errb.String())
		json, _ := json.Marshal(map[string]interface{}{
			"videos": "",
		})
		rw.Header().Set("Content-type", "application/json")
		rw.Write(json)
	})
}
