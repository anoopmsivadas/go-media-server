package handler

import (
	"encoding/json"
	"go-media-server/pkg/hls"
	"go-media-server/pkg/models"
	apiConfig "go-media-server/pkg/web/config"
	"log"
	"net/http"
	"strings"
)

func PrepareVideoHLS(config apiConfig.APIConfig) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {

		mediaId := strings.TrimSpace(r.URL.Query().Get("mediaId"))

		if mediaId == "" {
			response, _ := json.Marshal(map[string]interface{}{
				"processed": false,
			})

			rw.Write(response)
			rw.WriteHeader(http.StatusBadRequest)
		}

		var video models.Video

		if err := config.Connection.Where("id= ?", mediaId).First(&video).Error; err != nil {
			response, _ := json.Marshal(map[string]interface{}{
				"processed": false,
			})

			rw.Write(response)
			rw.WriteHeader(http.StatusNotFound)
		}

		if video.IsPrepared || video.IsInProcess {
			response, _ := json.Marshal(map[string]interface{}{
				"processed": true,
			})

			rw.Write(response)
			rw.WriteHeader(http.StatusOK)
		}

		// spawn worker to do extraction
		go func(config *apiConfig.APIConfig, video models.Video) {

			// extract
			hasErr, transErrs := hls.DoExtraction(&video, config)

			if hasErr {
				for _, terr := range transErrs {
					log.Fatalln("Trouble Transcoding Video["+terr.Resolution+"]", terr.Error)
				}
				return
			}

			log.Println("Extracting HLS finished:")

		}(&config, video)

		// spawn worker to detect .srt & convert .vtt
		//go collections.ProcessSrt(config.DB, &movie, config.AppDir)

		rw.Header().Set("content-type", "application/json")
		json, _ := json.Marshal(map[string]interface{}{
			"processed": true,
		})
		rw.Write(json)
		rw.WriteHeader(http.StatusOK)
	})
}
