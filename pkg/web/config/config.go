package config

import "gorm.io/gorm"

type APIConfig struct {
	Connection        *gorm.DB
	AppHome           string
	FFMPEGBin         string
	ScreenResolutions []string
}
