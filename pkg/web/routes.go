package web

import (
	"go-media-server/pkg/utils"
	apiConfig "go-media-server/pkg/web/config"
	"go-media-server/pkg/web/handler"
	"net/http"

	"gorm.io/gorm"
)

func MediaRouter(config *utils.ServerConfig, connection *gorm.DB) *http.ServeMux {
	router := http.NewServeMux()
	apiConfig := apiConfig.APIConfig{
		Connection:        connection,
		FFMPEGBin:         config.FFMPEGBin,
		ScreenResolutions: config.ScreenResolutions,
	}
	fs := http.Dir(config.MediaHome)

	router.Handle("/", http.StripPrefix("/", http.FileServer(fs)))
	router.Handle("/videos", handler.VideoHandler(apiConfig))
	router.Handle("/extract", handler.PrepareVideoHLS(apiConfig))

	return router
}
