package models

import "gorm.io/gorm"

type Video struct {
	gorm.Model
	FilePath      string  `json:"filePath"`
	FolderName    string  `json:"folderName"`
	FileName      string  `json:"baseName"`
	CleanFileName string  `json:"cleanBaseName"`
	FileSize      float64 `json:"fileSize"`
	MimeType      string  `json:"mimeType"`
	IsPrepared    bool    `json:"isPrepared"`
	IsInProcess   bool    `json:"isInPrepare"`
	IsSeries      bool    `json:"isGroupDir"`
}
