package service

import (
	"go-media-server/pkg/models"
	"go-media-server/pkg/scanner"
	"go-media-server/pkg/utils"
	"path/filepath"

	"gorm.io/gorm"
)

func SaveVideos(connection *gorm.DB, videos []scanner.MediaFile) {
	for _, video := range videos {
		dir := filepath.Base(video.Folder)

		baseNameParsedInfo, _ := utils.Parse(video.FileName)
		var existingRecord *models.Video
		if err := connection.Where(&models.Video{
			FilePath: video.Folder,
			FileName: video.FileName,
			FileSize: video.FileSize},
			"FilePath", "FileName", "FileSize").First(&existingRecord).Error; err != nil {
			connection.Create(&models.Video{
				FilePath:      video.Folder,
				FolderName:    dir,
				FileSize:      video.FileSize,
				FileName:      video.FileName,
				CleanFileName: baseNameParsedInfo.Title,
				MimeType:      video.MimeType,
				IsSeries:      false,
				IsPrepared:    false,
			})
		}
	}
}

// checks if a directory contains multiple videos and flags them as series
func isSeries(connection *gorm.DB, videos []scanner.MediaFile) {
	var videoSeries []models.Video

	connection.Table("videos").Select("file_path, COUNT(*) count").
		Group("file_path").
		Having("count > ?", 1).
		Find(&videoSeries)
	for _, video := range videoSeries {

		var videos []models.Video
		connection.Where(&models.Video{
			FilePath: video.FilePath},
			"file_path").Find(&videos)

		for _, vid := range videos {
			connection.Model(&vid).Update("is_series", true)
		}
	}

}
