package utils

import (
	"regexp"
	"strconv"
	"strings"
)

type FileInfo struct {
	Title string
	Year  int
}

func Parse(fileName string) (*FileInfo, error) {
	fileInfo := &FileInfo{}

	cleanName := strings.Replace(fileName, "_", " ", -1)
	// This is dumb, ik
	yearRe := regexp.MustCompile(`\b(((?:19[0-9]|20[0-9])[0-9]))\b`)
	matches := yearRe.FindAllStringSubmatch(cleanName, -1)

	fileInfo.Year, _ = strconv.Atoi(matches[len(matches)-1][1])

	cleanName = strings.Replace(fileName, strconv.Itoa(fileInfo.Year), "", -1)

	fileInfo.Title = cleanName

	return fileInfo, nil
}
