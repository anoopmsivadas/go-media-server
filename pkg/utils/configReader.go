package utils

import (
	"encoding/json"
	"log"
	"os"
	"path/filepath"
)

// TODO: use db for media home and support multiple
type ServerConfig struct {
	Port              int
	MediaHome         string
	FFMPEGBin         string
	DBPath            string
	ScreenResolutions []string
}

func LoadConfig(configPath string) (ServerConfig, error) {
	configFile, _ := os.Open(filepath.Join(configPath, "conf.json"))
	defer configFile.Close()
	decoder := json.NewDecoder(configFile)

	config := ServerConfig{}
	err := decoder.Decode(&config)

	if err != nil {
		log.Println(err)
	}

	return config, nil
}
