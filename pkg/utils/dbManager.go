package utils

import (
	"go-media-server/pkg/models"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func GetConnection(dbPath string) (*gorm.DB, error) {
	db, err := gorm.Open(sqlite.Open(dbPath+"test.db"), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return db, nil
}

func Migrate(db *gorm.DB) {
	db.AutoMigrate(
		&models.Video{},
	)
}
