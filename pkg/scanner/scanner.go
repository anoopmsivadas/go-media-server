package scanner

import (
	"io/ioutil"
	"math"
	"os"
	"path/filepath"

	"github.com/gabriel-vasile/mimetype"
)

type MediaFile struct {
	Folder   string
	FileName string
	FileSize float64
	Info     os.FileInfo
	MimeType string
}

// SubDirInfo is a holder to subtitle information
type SubTitle struct {
	Dir     string
	SubFile string
	Info    os.FileInfo
}

func ScanDir(path string) ([]MediaFile, []SubTitle, error) {
	var mediaFiles []MediaFile
	var subtitleFiles []SubTitle

	filelist, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, nil, err
	}

	for _, file := range filelist {
		detectedMovies, detectedSubs, err := FindMediaFiles(path, file)
		if err != nil {
			return nil, nil, err
		}

		mediaFiles = append(mediaFiles, detectedMovies...)
		subtitleFiles = append(subtitleFiles, detectedSubs...)
	}

	return mediaFiles, subtitleFiles, nil
}

func FindMediaFiles(basePath string, file os.FileInfo) ([]MediaFile, []SubTitle, error) {
	path := filepath.Join(basePath, file.Name())

	var mediaFiles []MediaFile
	var subtitleFiles []SubTitle

	if file.IsDir() {
		mediaFiles, subtitleFiles, err := ScanDir(path)
		return mediaFiles, subtitleFiles, err
	}

	if mime, validVideo := isVideo(path); validVideo {
		mediaFiles = append(mediaFiles, MediaFile{
			Folder:   basePath,
			FileName: file.Name(),
			FileSize: getFileSizeInMB(file),
			Info:     file,
			MimeType: mime,
		})
	}

	if validSub := IsTextSrt(path); validSub {
		subtitleFiles = append(subtitleFiles, SubTitle{
			Dir:     basePath,
			SubFile: file.Name(),
			Info:    file,
		})
	}

	return mediaFiles, subtitleFiles, nil
}

func getFileSizeInMB(file os.FileInfo) float64 {
	sizeInBytes := file.Size()
	sizeInMB := float64(sizeInBytes) / 1000000 // 1mio bytes = 1 MB
	sizeInMBRounded := math.Round(sizeInMB*100) / 100
	return sizeInMBRounded
}

func isVideo(path string) (string, bool) {
	validMimes := []string{
		"video/webm",       //.webm
		"video/3gpp",       // .3gp
		"video/3gpp2",      // .3g2
		"video/ogg",        // .ogv
		"video/mpeg",       // .mpeg or .mp4
		"video/x-msvideo",  // .avi
		"video/x-ms-wmv",   // .wmv
		"video/x-flv",      // .flv
		"video/mp4",        // .mp4
		"video/quicktime",  // .mov
		"video/x-matroska", // .mkv
		"video/x-ms-asf",   // .asf
		"video/x-m4v",      // .m4v
	}

	mime, err := mimetype.DetectFile(path)
	if err != nil {
		return "", false
	}

	listChecker := make(map[string]bool)
	for _, validMime := range validMimes {
		listChecker[validMime] = true
	}

	return mime.String(), listChecker[mime.String()]
}

// IsTextSrt will return valid status detection of .srt file
func IsTextSrt(path string) bool {
	mime, err := mimetype.DetectFile(path)
	if err != nil {
		return false
	}

	validMime := mime.String() == "text/plain; charset=utf-8"
	validExt := filepath.Ext(path) == ".srt" || filepath.Ext(path) == ".SRT"

	return validMime && validExt
}
