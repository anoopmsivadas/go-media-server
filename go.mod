module go-media-server

go 1.17

require (
	github.com/gabriel-vasile/mimetype v1.4.0
	gorm.io/driver/sqlite v1.2.6
	gorm.io/gorm v1.22.5
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.10 // indirect
	golang.org/x/net v0.0.0-20210505024714-0287a6fb4125 // indirect
)
