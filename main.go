package main

import (
	"go-media-server/pkg/scanner"
	"go-media-server/pkg/service"
	"go-media-server/pkg/utils"
	"go-media-server/pkg/web"
	"log"
	"net/http"
	"strconv"
)

//type HttpHandler struct{}
var serverConfig utils.ServerConfig

func main() {
	serverConfig, _ = utils.LoadConfig("./config")
	log.Println("Media Home: " + serverConfig.MediaHome)

	connection, err := utils.GetConnection(serverConfig.DBPath)

	if err != nil {
		panic("Database Connection Failed: " + err.Error())
	}

	utils.Migrate(connection)

	// Scan Media

	log.Println("Scanning for Media Files...")
	mediaFiles, _, _ := scanner.ScanDir(serverConfig.MediaHome)

	// Store it in DB
	service.SaveVideos(connection, mediaFiles)
	// Start Server
	webserver := http.Server{
		Addr:    ":" + strconv.Itoa(serverConfig.Port),
		Handler: web.MediaRouter(&serverConfig, connection),
	}

	log.Printf("Starting Server on Port %d", serverConfig.Port)
	err = webserver.ListenAndServe()

	if err != nil && err != http.ErrServerClosed {
		log.Fatalf("Unable to Start Server on Port: %d ", serverConfig.Port)
	}

	/*http.HandleFunc("/", rootHandler)
	rootDir := http.Dir("/home/anoop/Videos/Nolan")
	fs := http.FileServer(rootDir)

	http.Handle("/dir/", http.StripPrefix("/dir", fs))

	log.Fatal(http.ListenAndServe(":9000", nil))
	*/
}

/*
func rootHandler(res http.ResponseWriter, req *http.Request) {
	fmt.Fprint(res, "<h1>Test Server</h1>")
}
*/
/*func dirHandler(res http.ResponseWriter, req *http.Request) {
	header := res.Header()
	header.Set("Content-Type", "application/json")
	fmt.Fprintf(res, `{"message":"test"}`)
}*

/*
func (h HttpHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	data := []byte("Hello World")
	res.Write(data)
}*/
